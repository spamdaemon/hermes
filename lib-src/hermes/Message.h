#ifndef _HERMES_MESSAGE_H
#define _HERMES_MESSAGE_H

#ifndef _TIMBER_COUNTED_H
#include <timber/Counted.h>
#endif

namespace hermes {

   /**
    * The base class for all entities that can be transmitted
    * through Hermes.
    */
   class Message : public ::timber::Counted
   {
         CANOPY_BOILERPLATE_PREVENT_COPYING(Message)
         ;

         /** Protected constructor */
      protected:
         inline Message() throws ()
         {
         }

         /** The destructor */
      public:
         virtual ~Message() throws () = 0;

   };
}

#endif
