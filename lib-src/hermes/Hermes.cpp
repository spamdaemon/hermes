#include <hermes/Hermes.h>
#include <canopy/mt/Condition.h>
#include <canopy/mt/Mutex.h>
#include <canopy/mt/MutexGuard.h>
#include <canopy/mt/Thread.h>
#include <canopy/Static.h>
#include <timber/logging.h>
#include <timber/Reference.h>
#include <timber/Counted.h>
#include <timber/event/Dispatcher.h>
#include <deque>
#include <set>
#include <map>
#include <cassert>
#include <string>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::canopy::mt;

namespace hermes {
  namespace {

    struct MessageWrapper {
      MessageWrapper(const Reference<Message>& m)
	: _message(m) 
      {}

      void operator() (const Reference<Handler>& handler) throws()
      {
	try {
	  handler->messageAvailable(_message);
	}
	catch (const ::std::exception& e) {
	  LogEntry("hermes::Hermes",Level::EXCEPTION).stream()
	    << "Exception thrown by handler " << typeid(*handler).name() 
	    << " : " << e.what() 
	    << commit;
	}
	catch (...) {
	  LogEntry("hermes::Hermes",Level::EXCEPTION).stream()
	    << "Unknown thrown by handler " << typeid(*handler).name() 
	    << commit;
	}
      }

    private:
      Reference<Message> _message;
    };
    
    typedef ::std::deque<Reference<Message> > MessageQueue;
    typedef ::timber::event::Dispatcher<Reference<Handler> > Handlers;

    struct DispatcherWrapper : public Counted {
      DispatcherWrapper()
      {}

      ~DispatcherWrapper() throws()
      {}

       Handlers _handlers;
    };

  }

  /**
   * The implementation class maintains two sets of handlers.
   * Those that are typed, and those that are not. 
   */
  struct Hermes::Impl : public Hermes {

    Impl() throws ();
    ~Impl() throws ();

    static void hermesThread(void* hermesP) throws ();
    
    void mainLoop()  throws();
    void notifyHandlers (const Reference<Message>& m)  throws ();
    void enqueueMessage (const Reference<Message>& msg)  throws(::std::exception);
    void shutdown()  throws();
    void addHandler(const Reference<Handler>& handler)  throws(::std::exception);
    void addHandler(const Reference<Handler>& handler, const ::std::type_info& type)  throws(::std::exception);
    bool removeHandler(const Reference<Handler>& handler)  throws();
    bool removeHandler(const Reference<Handler>& handler, const ::std::type_info& type)  throws();
    
  private:
    Condition _queueMutex;

    /** The handler mutex */
  private:
    Mutex _handlerMutex;

    /** The handlers */
  private:
     Handlers _handlers;

    /**  A set of handlers, indexed by the message type */
  public:
    ::std::map< ::std::string, Reference<DispatcherWrapper> > _typedHandlers;

  private:
    MessageQueue _messageQueue;

  private:
    ::std::unique_ptr<Thread> _thread;

  private:
    bool _shutdown;
  };
 
  Hermes::Impl::Impl() throws()
    : _shutdown(false)
  {
    _thread.reset(new Thread("Hermes",hermesThread,this));
  }

  
  Hermes::Impl::~Impl() throws()
  {
    Log logger("hermes::Hermes");
    this->shutdown();
    Thread::join(*_thread);
  }

  void Hermes::Impl::hermesThread(void* hermesP) throws ()
  {
     Impl* H = reinterpret_cast<Impl*>(hermesP);
    H->mainLoop();
    H->_shutdown = true;
  }

  void Hermes::Impl::mainLoop()  throws()
  {
    Log logger("hermes::Hermes");

    MessageQueue localQueue;

    try {
      while(true) {
	// this inner loop polls for messages from the queue
	if(localQueue.empty()) {
	  MutexGuard<Condition> guard(_queueMutex);
	  
	  if (_shutdown) {
	    logger.info("Shutdown");
	    break;
	  }
	  
	  // allow for cancellation only after the local queue is empty
	  Thread::testCancelled();
	  
	  MessageQueue& queue = const_cast<MessageQueue&>(_messageQueue);
	  if (queue.empty()) {
	    logger.debugging("waiting for messages");
	    _queueMutex.wait();
	    logger.debugging("notified");
	  }
	  ::std::swap(localQueue,queue);
	  if (logger.isLoggable(Level::INFO)) {
	    LogEntry(logger,Level::INFO).stream() << "Number of of messages in queue " << localQueue.size() <<  commit;
	  }
	}
	else {
	  // deliver the message
	  notifyHandlers(localQueue.front());
	  localQueue.pop_front();
	}
      }
    }
    catch (const ::std::exception& e) {
      logger.caught("exception in Hermes mainloop", e);
     }
    catch (...) {
      logger.log(Level::CAUGHT,"Unknown exception");
    }
    if (!localQueue.empty()) {
      logger.debugging("Clearing local message queue");
      localQueue.clear();
    }
  }

  void Hermes::Impl::notifyHandlers (const Reference<Message>& m)  throws ()
  {
    MessageWrapper wrapper(m);
    // first, dispatch generic handlers
    _handlers.dispatch(wrapper);

    // dispatch handlers for a specific type of message
    {
      Pointer<DispatcherWrapper> typedHandlers;
      {
	MutexGuard<Mutex> guard(_handlerMutex);
	::std::map< ::std::string, Reference<DispatcherWrapper> >& map = const_cast< ::std::map< ::std::string, Reference<DispatcherWrapper> >&>(_typedHandlers);
	::std::map< ::std::string, Reference<DispatcherWrapper> >::iterator i = map.find(typeid(*m).name());
	if (i!=map.end()) {
	  typedHandlers = i->second;
	}
      }
      
      if (typedHandlers!=nullptr) {
	// note the handlers may be removed while a dispatch is in progress!
	typedHandlers->_handlers.dispatch(wrapper);
      }
    }
  }


  void Hermes::Impl::enqueueMessage (const Reference<Message>& msg)  throws(::std::exception)
  {
    MutexGuard<Condition> guard(_queueMutex);
    if (!_shutdown) {
      MessageQueue& queue = const_cast<MessageQueue&>(_messageQueue);
      LogEntry("hermes::Hermes",Level::INFO).stream() << "Enqueuing message of type " << typeid(*msg).name() << commit;
      queue.push_back(msg);
      _queueMutex.notify();
    }
  }

  void Hermes::Impl::shutdown()  throws()
  {
    MutexGuard<Condition> guard(_queueMutex);
    if (!_shutdown) {
      Thread* t = const_cast< ::std::unique_ptr<Thread>&>(_thread).get();
      _shutdown = true;
      Thread::cancel(*t);
      _queueMutex.notify();
    }
  }
  
  void Hermes::Impl::addHandler(const Reference<Handler>& handler)  throws(::std::exception)
  { _handlers.add(handler); }

  bool Hermes::Impl::removeHandler(const Reference<Handler>& handler)  throws() 
  {
    return _handlers.remove(handler); 
  }

  void Hermes::Impl::addHandler(const Reference<Handler>& handler, const ::std::type_info& type)  throws(::std::exception)
  {
    MutexGuard<Mutex> guard(_handlerMutex);
    ::std::map< ::std::string, Reference<DispatcherWrapper> >& map = const_cast< ::std::map< ::std::string, Reference<DispatcherWrapper> >&>(_typedHandlers);
    ::std::map< ::std::string, Reference<DispatcherWrapper> >::iterator i = map.find(type.name());
    if (i==map.end()) {
      Reference<DispatcherWrapper> handlers(new DispatcherWrapper());
      map.insert(::std::map< ::std::string, Reference<DispatcherWrapper> >::value_type(type.name(),handlers));
      // adding the handler here will not throw an exception
      handlers->_handlers.add(handler);
    }
    else {
      i->second->_handlers.add(handler);
    }
  }

  bool Hermes::Impl::removeHandler(const Reference<Handler>& handler, const ::std::type_info& type)  throws() 
  {
    MutexGuard<Mutex> guard(_handlerMutex);
    ::std::map< ::std::string, Reference<DispatcherWrapper> >& map = const_cast< ::std::map< ::std::string, Reference<DispatcherWrapper> >&>(_typedHandlers);
    ::std::map< ::std::string, Reference<DispatcherWrapper> >::iterator i = map.find(type.name());
    if (i==map.end()) {
      return false;
    }
    
    // we cannot cleanup here, because the dispatching may still be going on
    bool result = i->second->_handlers.remove(handler);
    if (i->second->_handlers.isEmpty()) {
      map.erase(i);
    }
    return result;
  }

  Hermes::Hermes() throws()
  {}

  Hermes::~Hermes() throws()
  {}

  Reference<Hermes> Hermes::create() throws()
  { return new Impl(); }

}
