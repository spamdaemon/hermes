#ifndef _HERMES_HANDLER_H
#define _HERMES_HANDLER_H

#ifndef _TIMBER_COUNTED_H
#include <timber/Counted.h>
#endif

#ifndef _TIMBER_REFERENCE_H
#include <timber/Reference.h>
#endif

namespace hermes {

  /** A message */
  class Message;

  /** 
   * A handler function that is invoked for each message
   * that is received.
   */
  class Handler : public ::timber::Counted {
    Handler&operator=(const Handler&);
    Handler(const Handler&);
    
    /** Destructor */
  public:
    ~Handler() throws ();
    
    /** The default constructor */
  protected:
    Handler() throws ();
    
    /** 
     * A message was received. If this method throws an exception, 
     * then this handler willl be removed. 
     * @param msg a message
     * @return true keep receiving messages, false to removed this handler
     */
  public:
    virtual void messageAvailable (::timber::Reference<Message> msg) = 0;
  };
}  

#endif
