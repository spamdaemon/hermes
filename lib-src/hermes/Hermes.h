#ifndef _HERMES_HERMES_H
#define _HERMES_HERMES_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _HERMES_MESSAGE_H
#include <hermes/Message.h>
#endif

#ifndef _HERMES_HANDLER_H
#include <hermes/Handler.h>
#endif

namespace hermes {

  /**
   * The Hermes messenger class.
   */
  class Hermes : public ::timber::Counted {

    /** The hermes implementation */
    class Impl;

    /**
     * Create a new Hermes instance.
     */
  protected:
    Hermes () throws ();

    /**
     * Destroy this hermes instance.
     */
  public:
    ~Hermes() throws ();

    /**
     * Get the single hermes instance. 
     * @return the hermes instance or 0 if it has already been destroyed
     */
  public:
    static ::timber::Reference<Hermes> create() throws ();

    /**
     * Shutdown hermes. Any messages enqueued after
     * this call will not be delivered
     */
  public:
    virtual void shutdown()  throws ()= 0;

    /**
     * Queue the message for delivery to all interested 
     * objects. The message will be delivered as soon as possible,
     * and it is even possible that it will be processed before
     * this method even has returned.
     * @param msg a message
     * @throws std::exception if the message could not be enqueued
     */
  public:
    virtual void enqueueMessage (const ::timber::Reference<Message>& msg)  throws ( ::std::exception) = 0;

    /**
     * Register the specified handler. This handler will be invoked for each message.
     * @param handler a handler
     * @throws exception if the handler was already registered
     */
  public:
    virtual void addHandler(const ::timber::Reference<Handler>& handler)  throws (::std::exception) = 0;

    /**
     * Add a handler for a specific kind of Message.
     * @param handler a handler
     * @param type the typeid for the message
     * @throws an exception if the handler was already registered for this type
     */
  public:
    virtual void addHandler(const ::timber::Reference<Handler>& handler, const ::std::type_info& type)  throws (::std::exception) = 0;

    /**
     * Remove a handler. 
     * @This function has no effect on handlers that were added with a message type.
     * @param handler a handler
     * @return true if the handler was removed, false if it did not exist
     */
  public:
    virtual bool removeHandler(const ::timber::Reference<Handler>& handler)  throws () = 0;

    /**
     * Remove a handler, but only for the specified type.
     * @param handler a handler
     * @param type the typeid 
     * @return true if the handler was removed, false if it did not exist
     */
  public:
    virtual bool removeHandler(const ::timber::Reference<Handler>& handler, const ::std::type_info& type)  throws () = 0;
    
  };
}

#endif
