#include <iostream>
#include <hermes/Hermes.h>
#include <timber/logging.h>
#include <canopy/mt/Thread.h>
#include <cassert>

using namespace hermes;
using namespace timber;
using namespace timber::logging;

static bool messageDeleted = false;

struct OtherMessage : public Message {

  OtherMessage() { }

  ~OtherMessage() throws() 
  {
  }

};

struct MyMessage : public Message {

  MyMessage() { messageDeleted = false; }

  ~MyMessage() throws() 
  {
    Log().info("Delete MyMessage"); 
    messageDeleted=true;
  }

};

struct MyHandler : public Handler {
  ~MyHandler() throws () {}

  void messageAvailable (Reference<Message> m)
  {
    LogEntry(Log(),Level::INFO).stream() << "Message received " << typeid(*m).name() << ::std::endl << commit;
  }

};

void utest_Hermes()
{
  //  Log("hermes::Hermes").setLevel(Level::INFO);
  {
    Reference<Message> m(new MyMessage());
    assert(!messageDeleted);
    Hermes::create()->enqueueMessage(m);
    ::canopy::mt::Thread::suspend(100000000);
  }
  assert(messageDeleted);
}

void utest_HermesHandler()
{
  Log("hermes::Hermes").setLevel(Level::INFO);
  Reference<Hermes> messenger = Hermes::create();
  Reference<Handler> cb(new MyHandler());
  messenger->addHandler(cb);
  
  {
    Reference<Message> m(new MyMessage());
    assert(!messageDeleted);
    messenger->enqueueMessage(m);
    ::canopy::mt::Thread::suspend(100000000);
  }
  assert(messageDeleted);

  messenger->removeHandler(cb);
}

void utest_HermesTypedHandler()
{
  Log("hermes::Hermes").setLevel(Level::INFO);
  Reference<Hermes> messenger = Hermes::create();
  Reference<Handler> cb(new MyHandler());
  messenger->addHandler(cb);
  messenger->addHandler(cb,typeid(MyMessage));
  
  {
    Reference<Message> m1(new MyMessage());
    Reference<Message> m2(new OtherMessage());
    assert(!messageDeleted);
    messenger->enqueueMessage(m1);
    messenger->enqueueMessage(m2);
    ::canopy::mt::Thread::suspend(100000000);
  }
  assert(messageDeleted);

  messenger->removeHandler(cb,typeid(MyMessage));  
  
}

